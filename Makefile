.POSIX:

PREFIX ?= /usr/local
SOURCE_FILES = www2md

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f www2md $(DESTDIR)$(PREFIX)/bin/www2md
	chmod 755 $(DESTDIR)$(PREFIX)/bin/www2md

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/www2md

lint:
	@shfmt -p -i 0 --diff $(SOURCE_FILES)
	@shellcheck $(SOURCE_FILES)

format:
	@shfmt -p -i 0 --write $(SOURCE_FILES)

.PHONY: install uninstall lint format
