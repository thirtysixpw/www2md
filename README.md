# www2md

Convert entire websites into markdown format for later usage
with [Hugo](https://gohugo.io/) static site generator or for
backup purposes.

## Installation

A makefile is provided to make install and removal easy.
Run `make install` *(as root if needed)*, `make uninstall` to remove.

## License

www2md is free/libre software. This program is released under the GPLv3
license, which you can find in the file [LICENSE](LICENSE).
